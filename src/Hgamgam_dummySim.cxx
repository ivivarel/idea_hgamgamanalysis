#include "Hgamgam_dummySim.h"

// ROOT includes 

#include <TMath.h>
#include <TH1F.h>
#include <TLorentzVector.h>

#include <iostream>

// The objective of this piece of code is to fold data about a particle decay of a Higgs boson with a detector resolution in order to obtain an expected shape for the H->gamgam decay

Hgamgam_dummySim::Hgamgam_dummySim(TString outfilename, double astoc, double bconst): m_outfilename(outfilename), m_file(0), m_a(astoc), m_b(bconst)
{}								 

bool Hgamgam_dummySim::RunDecays(unsigned int nevents)
{
  double p_higgs = 52.5;
  double m_higgs = 125;
  double E_higgs = TMath::Sqrt(TMath::Power(m_higgs,2) + TMath::Power(p_higgs,2));
  double beta =  p_higgs/E_higgs; // this is the beta of the boost needed to go from the Higgs CM to the lab frames

  m_file = new TFile(m_outfilename,"recreate");
  m_file->cd();
  TH1F * h_momGamma1 = new TH1F("h_momGamma1","",100,0,100);
  TH1F * h_momGamma2 = new TH1F("h_momGamma2","",100,0,100);

  TH1F * h_trueHiggsEnergy = new TH1F("h_trueHiggsEnergy","",100,0,200);
  TH1F * h_trueHiggsMom = new TH1F("h_trueHiggsMom","",100,0,100);
  TH1F * h_trueHiggsMass = new TH1F("h_trueHiggsMass","",100,115,135);

  TH1F * h_recoHiggsEnergy = new TH1F("h_recoHiggsEnergy","",100,0,200);
  TH1F * h_recoHiggsMom = new TH1F("h_recoHiggsMom","",100,0,100);
  TH1F * h_recoHiggsMass = new TH1F("h_recoHiggsMass","",100,115,135);
  
  for (unsigned int i = 0; i < nevents; ++i){
    
    // Find the random angle between the Higgs boost direction and the decay angle of one of the photons in the CM
    double costhetastar = 2*m_random.Rndm()-1.; // flat distribution in costheta
    double sinthetastar = TMath::Sqrt(1-TMath::Power(costhetastar,2));
    double phistar = 2*TMath::Pi()*m_random.Rndm() - TMath::Pi();
    
    // now define the 4-v4ctors of the two photons with respect to the boost direction
    
    TLorentzVector gamma1,gamma2;
    double E = m_higgs/2.;
    gamma1.SetPxPyPzE(E*sinthetastar*TMath::Cos(phistar),E*sinthetastar*TMath::Sin(phistar),E*costhetastar,E);
    gamma2.SetPxPyPzE(-E*sinthetastar*TMath::Cos(phistar),-E*sinthetastar*TMath::Sin(phistar),-E*costhetastar,E);
    // we are about to boost along the z axis (coinciding with the Higgs boost axis in the lab). We need beta to do that
    gamma1.Boost(0,0,beta);
    gamma2.Boost(0,0,beta);
    
    // Make sure the lab kinematics is reasonable

    h_momGamma1->Fill(gamma1.P());
    h_momGamma2->Fill(gamma2.P());
    
    TLorentzVector higgsTrue = gamma1+gamma2;
    h_trueHiggsEnergy->Fill(higgsTrue.E());
    h_trueHiggsMom->Fill(higgsTrue.P());
    h_trueHiggsMass->Fill(higgsTrue.M());

    TLorentzVector recoGamma1, recoGamma2, higgsReco;

    double reco1Ene = m_random.Gaus(gamma1.E(),E_sigma(gamma1.E()));
    double reco2Ene = m_random.Gaus(gamma2.E(),E_sigma(gamma2.E()));

    recoGamma1 =  (reco1Ene/gamma1.E()) * gamma1;
    recoGamma2 =  (reco2Ene/gamma2.E()) * gamma2;
    higgsReco = recoGamma1 + recoGamma2;
    
    h_recoHiggsEnergy->Fill(higgsReco.E());
    h_recoHiggsMom->Fill(higgsReco.P());
    h_recoHiggsMass->Fill(higgsReco.M()); 
  }
  m_file->Write();
  return true;
}

double Hgamgam_dummySim::E_sigma(double E)
{
  return E * (TMath::Sqrt(TMath::Power(m_a /TMath::Sqrt(E),2) + TMath::Power(m_b,2)));
}
