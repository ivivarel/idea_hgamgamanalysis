#define HgamgamAnalysis_cxx
#include "HgamgamAnalysis.h"



#include <TH1F.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TMath.h>

#include <iostream>

bool HgamgamAnalysis::Loop()
{
  if (fChain == 0) {
    std::cerr << "Cannot start loop. Was the analysis class properly initialised?" << std::endl;
    return false;
  }

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
     //  for (Long64_t jentry=0; jentry<200000;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;

      if (jentry%10000 == 0) std::cout << jentry << " events processed so far" << std::endl;

      m_eventWeight = 1;

      m_sel.Reset();

      if (!LoadEvent()) {
	std::cout << "Cannot load event" << std::endl;
	return false;
      }

      
      for (Cut i : m_sel.GetVector()){
	if (!IsPassed(i)) break;
	m_histo_manager->h_eventInfo_unw->Fill(i.GetIndex()+0.5);
	m_histo_manager->h_eventInfo->Fill(i.GetIndex()+0.5,m_eventWeight);
	this->FillHistograms(i);
      }
      
      // if (Cut(ientry) < 0) continue;
   } // end of loop over events
   return true;
}

HgamgamAnalysis::HgamgamAnalysis(TString outfilename, bool debug) : fChain(0), m_outfilename(outfilename), m_debug(debug), m_outHIST(0),m_frecal(0),m_theta_cal(0),h_eshare_scint1(0),h_eshare_scint2(0),h_eshare_cher1(0),h_eshare_cher2(0),h_thetacal_scint1(0),h_thetacal_scint2(0),h_thetacal_cher1(0),h_thetacal_cher2(0)
{
  
  // Informations about the geometry

  m_dtheta_deg = 45./40.;
  m_dphi_deg = 360./36.;
  
}

HgamgamAnalysis::~HgamgamAnalysis()
{

  m_outHIST->Write();
  m_outHIST->Close();

  delete m_outHIST;  
}

Int_t HgamgamAnalysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}

Long64_t HgamgamAnalysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
   }
   return centry;
}

bool HgamgamAnalysis::Init(TTree *tree)
{

  // Load Calibration eshare histograms
  m_frecal = TFile::Open("calfile_DR_hgamgam_check.root");
  if (!m_frecal) return false;
  h_eshare_scint1 = (TProfile *) m_frecal->Get("h_calib_scint1_pfx");
  h_eshare_scint2 = (TProfile *) m_frecal->Get("h_calib_scint2_pfx");
  h_eshare_cher1 = (TProfile *) m_frecal->Get("h_calib_cher1_pfx");
  h_eshare_cher2 = (TProfile *) m_frecal->Get("h_calib_cher2_pfx");
  m_theta_cal = TFile::Open("calfile_step2_DR_hgamgam_check.root");
  if (!m_theta_cal) return false;
  h_thetacal_scint1 = (TProfile *) m_theta_cal->Get("h_profile_scint1_R");
  h_thetacal_scint2 = (TProfile *) m_theta_cal->Get("h_profile_scint2_R");
  h_thetacal_cher1 = (TProfile *) m_theta_cal->Get("h_profile_cher1_R");
  h_thetacal_cher2 = (TProfile *) m_theta_cal->Get("h_profile_cher2_R");
  
   if (!tree) return false;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);
   fChain->SetBranchStatus("*",0);

   fChain->SetBranchAddress("j1t_E", &j1t_E, &b_j1t_E);
   fChain->SetBranchAddress("j1t_pt", &j1t_pt, &b_j1t_pt);
   fChain->SetBranchAddress("j1t_eta", &j1t_eta, &b_j1t_eta);
   fChain->SetBranchAddress("j1t_phi", &j1t_phi, &b_j1t_phi);
   fChain->SetBranchAddress("j1t_theta", &j1t_theta, &b_j1t_theta);
   fChain->SetBranchAddress("j2t_E", &j2t_E, &b_j2t_E);
   fChain->SetBranchAddress("j2t_pt", &j2t_pt, &b_j2t_pt);
   fChain->SetBranchAddress("j2t_eta", &j2t_eta, &b_j2t_eta);
   fChain->SetBranchAddress("j2t_phi", &j2t_phi, &b_j2t_phi);
   fChain->SetBranchAddress("j2t_theta", &j2t_theta, &b_j2t_theta);
   fChain->SetBranchAddress("j1s_E", &j1s_E, &b_j1s_E);
   fChain->SetBranchAddress("j1s_pt", &j1s_pt, &b_j1s_pt);
   fChain->SetBranchAddress("j1s_eta", &j1s_eta, &b_j1s_eta);
   fChain->SetBranchAddress("j1s_phi", &j1s_phi, &b_j1s_phi);
   fChain->SetBranchAddress("j2s_E", &j2s_E, &b_j2s_E);
   fChain->SetBranchAddress("j2s_pt", &j2s_pt, &b_j2s_pt);
   fChain->SetBranchAddress("j2s_eta", &j2s_eta, &b_j2s_eta);
   fChain->SetBranchAddress("j2s_phi", &j2s_phi, &b_j2s_phi);
   fChain->SetBranchAddress("j1c_E", &j1c_E, &b_j1c_E);
   fChain->SetBranchAddress("j1c_pt", &j1c_pt, &b_j1c_pt);
   fChain->SetBranchAddress("j1c_eta", &j1c_eta, &b_j1c_eta);
   fChain->SetBranchAddress("j1c_phi", &j1c_phi, &b_j1c_phi);
   fChain->SetBranchAddress("j2c_E", &j2c_E, &b_j2c_E);
   fChain->SetBranchAddress("j2c_pt", &j2c_pt, &b_j2c_pt);
   fChain->SetBranchAddress("j2c_eta", &j2c_eta, &b_j2c_eta);
   fChain->SetBranchAddress("j2c_phi", &j2c_phi, &b_j2c_phi);
   fChain->SetBranchAddress("j1s_eshare", &j1s_eshare, &b_j1s_eshare);
   fChain->SetBranchAddress("j2s_eshare", &j2s_eshare, &b_j2s_eshare);
   fChain->SetBranchAddress("j1c_eshare", &j1c_eshare, &b_j1c_eshare);
   fChain->SetBranchAddress("j2c_eshare", &j2c_eshare, &b_j2c_eshare);

   this->InitCuts();
   TString l_histfilename = m_outfilename + "_HIST.root";
   std::cout << "Opening output histogram file with name " << l_histfilename << std::endl;

   m_outHIST = new TFile(l_histfilename,"recreate");
   if (!m_outHIST->IsOpen()) {
     std::cerr << "Cannot open file with name " << l_histfilename << ". Aborting..." << std::endl;
     return false;
   }

   m_histo_manager = new AnalysisHistoBase((*m_outHIST),m_sel);
   if (!m_histo_manager->Init()){
     std::cout << "ERROR! Cannot initialise m_histo_manager" << std::endl;
   }

   this->InitHistograms();

   std::cout << "Initialisation complete" << std::endl;

   return true;
   
}

bool HgamgamAnalysis::FillHistograms(Cut i)
{
  TH1F * h = 0;
  TH2F * h2 = 0;

  h = (TH1F *) m_histo_manager->GetObject(i,"h_mass");
  h->Fill(h1.M(),m_eventWeight);

  h = (TH1F *) m_histo_manager->GetObject(i,"h_p");
  h->Fill(h1.P(),m_eventWeight);

  h = (TH1F *) m_histo_manager->GetObject(i,"h_p_true");
  h->Fill(h1t.P(),m_eventWeight);

  h = (TH1F *) m_histo_manager->GetObject(i,"h_E1");
  h->Fill(r1.E(),m_eventWeight);
  
  h = (TH1F *) m_histo_manager->GetObject(i,"h_E2");
  h->Fill(r2.E(),m_eventWeight);
  
  h = (TH1F *) m_histo_manager->GetObject(i,"h_mass_scin");
  h->Fill(h1_s.M(),m_eventWeight);
  
  h = (TH1F *) m_histo_manager->GetObject(i,"h_mass_cher");
  h->Fill(h1_c.M(),m_eventWeight);
  
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_finescan_scint1_R");
  h2->Fill(t1.Theta(),s1.E()/t1.E(),m_eventWeight);
 
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_finescan_cher1_R");
  h2->Fill(t1.Theta(),c1.E()/t1.E(),m_eventWeight);
 
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_finescan_scint2_R");
  h2->Fill(t2.Theta(),s2.E()/t2.E(),m_eventWeight);
  
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_finescan_cher2_R");
  h2->Fill(t2.Theta(),c2.E()/t2.E(),m_eventWeight);

  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_finephiscan_scint1_R");
  h2->Fill(t1.Phi(),s1.E()/t1.E(),m_eventWeight);
 
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_finephiscan_cher1_R");
  h2->Fill(t1.Phi(),c1.E()/t1.E(),m_eventWeight);
 
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_finephiscan_scint2_R");
  h2->Fill(t2.Phi(),s2.E()/t2.E(),m_eventWeight);
  
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_finephiscan_cher2_R");
  h2->Fill(t2.Phi(),c2.E()/t2.E(),m_eventWeight);

    h2 = (TH2F *) m_histo_manager->GetObject(i,"h_fineEscan_scint1_R");
  h2->Fill(t1.E(),s1.E()/t1.E(),m_eventWeight);
 
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_fineEscan_cher1_R");
  h2->Fill(t1.E(),c1.E()/t1.E(),m_eventWeight);
 
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_fineEscan_scint2_R");
  h2->Fill(t2.E(),s2.E()/t2.E(),m_eventWeight);
  
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_fineEscan_cher2_R");
  h2->Fill(t2.E(),c2.E()/t2.E(),m_eventWeight);
 
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_col_scint1_R");
  h2->Fill(t1.Theta(),s1.E()/t1.E(),m_eventWeight);
  
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_col_scint2_R");
  h2->Fill(t2.Theta(),s2.E()/t2.E(),m_eventWeight);
  
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_col_cher1_R");
  h2->Fill(t1.Theta(),c1.E()/t1.E(),m_eventWeight);
  
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_col_cher2_R");
  h2->Fill(t2.Theta(),c2.E()/t2.E(),m_eventWeight);
  
  h = (TH1F *) m_histo_manager->GetObject(i,"h_mass_hyb_ene");
  h->Fill(hybh_tD.M(),m_eventWeight);

    h = (TH1F *) m_histo_manager->GetObject(i,"h_mass_hyb_ene_scint");
  h->Fill(hybh_tD_scint.M(),m_eventWeight);

    h = (TH1F *) m_histo_manager->GetObject(i,"h_mass_hyb_ene_cher");
  h->Fill(hybh_tD_cher.M(),m_eventWeight);
  
  h = (TH1F *) m_histo_manager->GetObject(i,"h_mass_hyb_dir");
  h->Fill(hybh_tE.M(),m_eventWeight);
  
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_calib_scint1");
  h2->Fill(j1s_eshare,s1.E()/t1.E(),m_eventWeight);
  
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_calib_scint2");
  h2->Fill(j2s_eshare,s2.E()/t2.E(),m_eventWeight);
  
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_calib_cher1");
  h2->Fill(j1c_eshare,c1.E()/t1.E(),m_eventWeight);
    
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_calib_cher2");
  h2->Fill(j2c_eshare,c2.E()/t2.E(),m_eventWeight);

  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_residual_E_60GeV");
  h2->Fill(GetResidualThetaDeg(t1.Theta()),GetResidualPhiDeg(t1.Phi()),m_eventWeight* r1.E()/t1.E());
  h2->Fill(GetResidualThetaDeg(t2.Theta()),GetResidualPhiDeg(t2.Phi()),m_eventWeight* r2.E()/t2.E());
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_residual_E_60GeV_scin");
  h2->Fill(GetResidualThetaDeg(t1.Theta()),GetResidualPhiDeg(t1.Phi()),m_eventWeight* s1.E()/t1.E());
  h2->Fill(GetResidualThetaDeg(t2.Theta()),GetResidualPhiDeg(t2.Phi()),m_eventWeight* s2.E()/t2.E());
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_residual_E_60GeV_cher");
  h2->Fill(GetResidualThetaDeg(t1.Theta()),GetResidualPhiDeg(t1.Phi()),m_eventWeight* c1.E()/t1.E());
  h2->Fill(GetResidualThetaDeg(t2.Theta()),GetResidualPhiDeg(t2.Phi()),m_eventWeight* c2.E()/t2.E());
  h2 = (TH2F *) m_histo_manager->GetObject(i,"h_residual_E_60GeV_entries");
  h2->Fill(GetResidualThetaDeg(t1.Theta()),GetResidualPhiDeg(t1.Phi()),m_eventWeight);
  h2->Fill(GetResidualThetaDeg(t2.Theta()),GetResidualPhiDeg(t2.Phi()),m_eventWeight);

  h = (TH1F *) m_histo_manager->GetObject(i,"h_tower10_E_60GeV");
  if (t1.E() > 57.5 && t1.E() < 62.5 && TMath::Abs((t1.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) > (10.*m_dtheta_deg) && TMath::Abs((t1.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) < (11.*m_dtheta_deg)) h->Fill(r1.E()/t1.E(),m_eventWeight);
  if (t2.E() > 57.5 && t2.E() < 62.5 && TMath::Abs((t2.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) > (10.*m_dtheta_deg) && TMath::Abs((t2.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) < (11.*m_dtheta_deg)) h->Fill(r2.E()/t2.E(),m_eventWeight);

  h = (TH1F *) m_histo_manager->GetObject(i,"h_tower10_E_60GeV_scin");
  if (t1.E() > 57.5 && t1.E() < 62.5 && TMath::Abs((t1.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) > (10.*m_dtheta_deg) && TMath::Abs((t1.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) < (11.*m_dtheta_deg)) h->Fill(s1.E()/t1.E(),m_eventWeight);
  if (t2.E() > 57.5 && t2.E() < 62.5 && TMath::Abs((t2.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) > (10.*m_dtheta_deg) && TMath::Abs((t2.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) < (11.*m_dtheta_deg)) h->Fill(s2.E()/t2.E(),m_eventWeight);

  h = (TH1F *) m_histo_manager->GetObject(i,"h_tower10_E_60GeV_cher");
  if (t1.E() > 57.5 && t1.E() < 62.5 && TMath::Abs((t1.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) > (10.*m_dtheta_deg) && TMath::Abs((t1.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) < (11.*m_dtheta_deg)) h->Fill(c1.E()/t1.E(),m_eventWeight);
  if (t2.E() > 57.5 && t2.E() < 62.5 && TMath::Abs((t2.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) > (10.*m_dtheta_deg) && TMath::Abs((t2.Theta()-TMath::Pi()/2.)*TMath::RadToDeg()) < (11.*m_dtheta_deg)) h->Fill(c2.E()/t2.E(),m_eventWeight);
  

  // Scan the coefficient for combining the cherenkov and scintillation

  // The idea is that the the two jets could be combined using a weighted average.
  // The only thing that matters for the combination of two objects is the ratio between the two weights.

  // (aV1 + bV2)/(a+b) = (V1 + b/a V2)/(1+b/a)

  TLorentzVector r1_coeff,r2_coeff,h_coeff;
  std::vector<TString> v_coeff_name = {"2.50","2.25","2.00","1.75","1.50","1.25","1.00","0.75","0.50","0.25"};
  for (auto coeff_name : v_coeff_name){
    h = (TH1F *) m_histo_manager->GetObject(i,"h_mass_CoverS_" + coeff_name);
    r1_coeff = MakeHybrid(CombineJets(s1,c1,1.,coeff_name.Atof()),t1);
    r2_coeff = MakeHybrid(CombineJets(s2,c2,1.,coeff_name.Atof()),t2);
    h_coeff = r1_coeff + r2_coeff;
    h->Fill(h_coeff.M(),m_eventWeight);
  }
  
  return true;
}


bool HgamgamAnalysis::IsPassed(Cut& l_cut)
{
  switch(l_cut.GetIndex()){
  case 0:
    l_cut.SetPassed(true);
    break;
  case 1:
    l_cut.SetPassed(j1t_theta > 0.7 && j1t_theta < 2.4 && j2t_theta > 0.7 && j2t_theta < 2.4);
    break;
  case 2:
    l_cut.SetPassed(j1t_E > 10. && j2t_E > 10.);
    break;
  case 3:
    l_cut.SetPassed(j1s_E > 10. && j2s_E > 10. && j1c_E > 10. && j2c_E > 10.);
    break;
  case 4:
    l_cut.SetPassed(j1s_E > 40. && j2s_E > 40. && j1c_E > 40. && j2c_E > 40.);
    break;
  case 5:
    l_cut.SetPassed(
		    TMath::Abs(GetResidualThetaDeg(t1.Theta())) < 0.35 &&
		    TMath::Abs(GetResidualThetaDeg(t2.Theta())) < 0.35 &&
		    TMath::Abs(GetResidualPhiDeg(t1.Phi())) > 0.5 &&
		    TMath::Abs(GetResidualPhiDeg(t2.Phi())) > 0.5);
    break;
  }
  return l_cut.IsPassed();
}

bool HgamgamAnalysis::InitCuts()
{
  m_sel.AddCut(0,"Inclusive");
  m_sel.AddCut(1,"TruthThetaAcceptance");
  m_sel.AddCut(2,"TruthEnergy");
  m_sel.AddCut(3,"RecoEnergy");
  m_sel.AddCut(4,"IncreasedRecoEnergy");
  m_sel.AddCut(5,"CentreTower");
  return true;


}

bool HgamgamAnalysis::InitHistograms()
{

  m_histo_manager->AddTH1F("h_p","",150,0,150);
  m_histo_manager->AddTH1F("h_p_true","",150,0,150);
  m_histo_manager->AddTH1F("h_E1","",150,0,150);
  m_histo_manager->AddTH1F("h_E2","",150,0,150);
  m_histo_manager->AddTH1F("h_mass","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_fromJets","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_scin","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_cher","",160,110,150);
  m_histo_manager->AddTH2F("h_col_scint1_R", "", 32,0,3.2,40,0.5,1.5);
  m_histo_manager->AddTH2F("h_col_cher1_R", "", 32,0,3.2,40,0.5,1.5);
  m_histo_manager->AddTH2F("h_col_scint2_R", "", 32,0,3.2,40,0.5,1.5);
  m_histo_manager->AddTH2F("h_col_cher2_R", "", 32,0,3.2,40,0.5,1.5);

  // Scan how to best combine the cherenkov and scintillating jets

  m_histo_manager->AddTH1F("h_mass_CoverS_2.50","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_CoverS_2.25","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_CoverS_2.00","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_CoverS_1.75","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_CoverS_1.50","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_CoverS_1.25","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_CoverS_1.00","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_CoverS_0.75","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_CoverS_0.50","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_CoverS_0.25","",160,110,150);
  
  // Fine eta scan

  m_histo_manager->AddTH2F("h_finescan_scint1_R","",68,0.7,2.4,30,0.85,1.15);
  m_histo_manager->AddTH2F("h_finescan_cher1_R","",68,0.7,2.4,30,0.85,1.15);
  m_histo_manager->AddTH2F("h_finescan_scint2_R","",68,0.7,2.4,30,0.85,1.15);
  m_histo_manager->AddTH2F("h_finescan_cher2_R","",68,0.7,2.4,30,0.85,1.15);

    // Fine phi scan

  m_histo_manager->AddTH2F("h_finephiscan_scint1_R","",72,-TMath::Pi(),TMath::Pi(),30,0.85,1.15);
  m_histo_manager->AddTH2F("h_finephiscan_cher1_R","",72,-TMath::Pi(),TMath::Pi(),30,0.85,1.15);
  m_histo_manager->AddTH2F("h_finephiscan_scint2_R","",72,-TMath::Pi(),TMath::Pi(),30,0.85,1.15);
  m_histo_manager->AddTH2F("h_finephiscan_cher2_R","",72,-TMath::Pi(),TMath::Pi(),30,0.85,1.15);

    // Fine energy scan

  m_histo_manager->AddTH2F("h_fineEscan_scint1_R","",45,30,120,30,0.85,1.15);
  m_histo_manager->AddTH2F("h_fineEscan_cher1_R","",45,30,120,30,0.85,1.15);
  m_histo_manager->AddTH2F("h_fineEscan_scint2_R","",45,30,120,30,0.85,1.15);
  m_histo_manager->AddTH2F("h_fineEscan_cher2_R","",45,30,120,30,0.85,1.15);

  
  m_histo_manager->AddTH1F("h_mass_hyb_ene","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_hyb_ene_scint","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_hyb_ene_cher","",160,110,150);
  m_histo_manager->AddTH1F("h_mass_hyb_dir","",160,110,150);
  m_histo_manager->AddTH2F("h_calib_scint1","",200,-1,1,40,0.5,1.5);
  m_histo_manager->AddTH2F("h_calib_scint2","",200,-1,1,40,0.5,1.5);
  m_histo_manager->AddTH2F("h_calib_cher1","",200,-1,1,40,0.5,1.5);
  m_histo_manager->AddTH2F("h_calib_cher2","",200,-1,1,40,0.5,1.5);

  // Cell maps

  m_histo_manager->AddTH2F("h_residual_E_60GeV_scin","",100,-0.8,0.8,100,-7,7);
  m_histo_manager->AddTH2F("h_residual_E_60GeV_cher","",100,-0.8,0.8,100,-7,7);
  m_histo_manager->AddTH2F("h_residual_E_60GeV","",100,-0.8,0.8,100,-7,7);
  m_histo_manager->AddTH2F("h_residual_E_60GeV_entries","",100,-0.8,0.8,100,-7,7);
  m_histo_manager->AddTH1F("h_tower10_E_60GeV","",100,0.5,1.5);
  m_histo_manager->AddTH1F("h_tower10_E_60GeV_cher","",100,0.5,1.5);
  m_histo_manager->AddTH1F("h_tower10_E_60GeV_scin","",100,0.5,1.5);
  
  
  return true;
}

bool HgamgamAnalysis::LoadEvent()
{

  // The photon four vector has to be redefined starting from the jet 4-vector, but actually setting its mass to zero. 

  t1 = CreatePhoton4Vec(j1t_pt,j1t_eta,j1t_phi,j1t_E);
  s1 = CreatePhoton4Vec(j1s_pt,j1s_eta,j1s_phi,j1s_E);
  c1 = CreatePhoton4Vec(j1c_pt,j1c_eta,j1c_phi,j1c_E);
  t2 = CreatePhoton4Vec(j2t_pt,j2t_eta,j2t_phi,j2t_E);
  s2 = CreatePhoton4Vec(j2s_pt,j2s_eta,j2s_phi,j2s_E);
  c2 = CreatePhoton4Vec(j2c_pt,j2c_eta,j2c_phi,j2c_E);
  
  
  if (!Calibrate_Cal1()){
    return false;
  }

  if (!Calibrate_Cal2()){
    return false;
  }
  
  r1 = CombineJets(s1,c1);
  r2 = CombineJets(s2,c2);
  h1t = t1 + t2;
  h1 = r1 + r2;
  h1_s = s1 + s2;
  h1_c = c1 + c2;
  
  hybh_tE = (MakeHybrid(t1,r1) + MakeHybrid(t2,r2));
  hybh_tD = (MakeHybrid(r1,t1) + MakeHybrid(r2,t2));
  hybh_tD_scint = (MakeHybrid(s1,t1) + MakeHybrid(s2,t2));
  hybh_tD_cher = (MakeHybrid(c1,t1) + MakeHybrid(c2,t2));

  return true;
}

bool HgamgamAnalysis::Calibrate_Cal1()
{
  
  static double CalConst;
  
  if (!h_eshare_scint1) return false;
  CalConst = h_eshare_scint1->GetBinContent(h_eshare_scint1->FindBin(j1s_eshare));
  
  if (CalConst != 0) CalConst = 1./CalConst;
  else CalConst = 1.;

  s1 = CalConst * s1;

  if (!h_eshare_scint2) return false;
  CalConst = h_eshare_scint2->GetBinContent(h_eshare_scint2->FindBin(j2s_eshare));
  
  if (CalConst != 0) CalConst = 1./CalConst;
  else CalConst = 1.;

  s2 = CalConst * s2;

  if (!h_eshare_cher1) return false;
  CalConst = h_eshare_cher1->GetBinContent(h_eshare_cher1->FindBin(j1c_eshare));
  
  if (CalConst != 0) CalConst = 1./CalConst;
  else CalConst = 1.;

  c1 = CalConst * c1;

  if (!h_eshare_cher2) return false;
  CalConst = h_eshare_cher2->GetBinContent(h_eshare_cher2->FindBin(j2c_eshare));
  
  if (CalConst != 0) CalConst = 1./CalConst;
  else CalConst = 1.;

  c2 = CalConst * c2;

  return true;
}

bool HgamgamAnalysis::Calibrate_Cal2()
{
  static double CalConst;
  
  if (!h_thetacal_scint1) return false;
  CalConst = h_thetacal_scint1->GetBinContent(h_thetacal_scint1->FindBin(j1t_theta));
  
  if (CalConst != 0) CalConst = 1./CalConst;
  else CalConst = 1.;

  s1 = CalConst * s1;

  if (!h_thetacal_scint2) return false;
  CalConst = h_thetacal_scint2->GetBinContent(h_thetacal_scint2->FindBin(j2s_theta));
  
  if (CalConst != 0) CalConst = 1./CalConst;
  else CalConst = 1.;

  s2 = CalConst * s2;

  if (!h_thetacal_cher1) return false;
  CalConst = h_thetacal_cher1->GetBinContent(h_thetacal_cher1->FindBin(j1c_theta));
  
  if (CalConst != 0) CalConst = 1./CalConst;
  else CalConst = 1.;

  c1 = CalConst * c1;

  if (!h_thetacal_cher2) return false;
  CalConst = h_thetacal_cher2->GetBinContent(h_thetacal_cher2->FindBin(j2c_theta));
  
  if (CalConst != 0) CalConst = 1./CalConst;
  else CalConst = 1.;

  c2 = CalConst * c2;

  
  return true;
}

TLorentzVector HgamgamAnalysis::CombineJets(TLorentzVector v1, TLorentzVector v2, double c_v1, double c_v2 )
{
  v1 = c_v1*v1;
  v2 = c_v2*v2;
  return  (v1 + v2) *(1/(c_v1+c_v2)); 
}

 TLorentzVector HgamgamAnalysis::MakeHybrid(TLorentzVector mag, TLorentzVector direction)
{
  TLorentzVector retval(0.,0.,0.,0.);
  
  double magnitude = mag.E();
  double dirMag = TMath::Sqrt(TMath::Power(direction.Px(),2) +
                              TMath::Power(direction.Py(),2) +
                              TMath::Power(direction.Pz(),2));

  if (TMath::Abs(dirMag) < 0.000001) {
    std::cout << "Warning null magnitude, returning 0" << std::endl;
    return retval;
  }

  
  retval.SetPxPyPzE(magnitude * direction.Px()/dirMag, magnitude * direction.Py()/dirMag, magnitude * direction.Pz()/dirMag, magnitude);

  return retval;
}

TLorentzVector HgamgamAnalysis::CreatePhoton4Vec(double pt, double eta, double phi, double E)
{
  TLorentzVector retval(0.,0.,0.,0.);

  // The Logic is to maintain the energy as spacial magnitude, so that the mass is zero

  retval.SetPtEtaPhiE(pt,eta,phi,E);
  double Mom3 = retval.P();
  retval.SetPxPyPzE(
		    retval.Px() * E/Mom3,
		    retval.Py() * E/Mom3,
		    retval.Pz() * E/Mom3,
		    E);

  return retval;
}

double HgamgamAnalysis::GetResidualThetaDeg(double theta)
{
 
  double absthetadeg = TMath::Abs(theta*TMath::RadToDeg());
  unsigned int intdiv = absthetadeg/m_dtheta_deg;
  //  std::cout << absthetadeg << "    " << absthetadeg - (((double) intdiv) * m_dtheta_deg + 0.5 * m_dtheta_deg) << std::endl;
  return absthetadeg - (((double) intdiv) * m_dtheta_deg + 0.5 * m_dtheta_deg);
}

double HgamgamAnalysis::GetResidualPhiDeg(double phi)
{
  double absphideg = TMath::Abs(phi*TMath::RadToDeg());
  unsigned int intdiv = absphideg/m_dphi_deg;
  return absphideg - (((double) intdiv) * m_dphi_deg + 0.5 * m_dphi_deg);
}


    
      
