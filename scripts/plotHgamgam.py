#! /usr/bin/env python

import ROOT

import os,math,glob
import errno
from array import  array

def main():
        import argparse

        #Parse arguments from the command line
        parser = argparse.ArgumentParser(description='GenerateHgamgamFileLists.py: a script to produce txt file containing chunks of nfiles files from a directory maindir')
        parser.add_argument('--input_file',action='store',dest='input_file',default="Output_HIST.root",help='Input file name')
        parser.add_argument('--selection_level',action='store',dest='selection',default='Cut3_RecoEnergy',help='Selection level in the input file')
        parser.add_argument('--doCalib1File', action='store_true',dest='doCalib1',default=False,help='Prepare calibration file step 1')
        parser.add_argument('--doCalib2File', action='store_true',dest='doCalib2',default=False,help='Prepare calibration file step 2')

        par = parser.parse_args()

        ifile = ROOT.TFile(par.input_file)
        if not ifile.IsOpen():
            print 'Cannot find file ' + par.input_file
            return

        c = ROOT.TCanvas()
        c.SetRightMargin(0.15)

        ROOT.gStyle.SetOptStat(1)
        ROOT.gStyle.SetOptFit(11111)

        #### generic histograms about event kinematics
        
        h_p = ifile.Get(par.selection + "/h_p")
        h_p.SetXTitle("p_{H} [GeV]")
        h_p.Draw("HIST")
        c.Print("RecoHiggsMomenum.pdf")

        h_E1 = ifile.Get(par.selection + "/h_E1")
        h_E1.SetXTitle("E_{\gamma 1} [GeV]")
        h_E1.Draw("HIST")
        c.Print("RecoPhotonE1.pdf")

        h_E2 = ifile.Get(par.selection + "/h_E2")
        h_E2.SetXTitle("E_{\gamma 2} [GeV]")
        h_E2.Draw("HIST")
        c.Print("RecoPhotonE2.pdf")

        ROOT.gStyle.SetOptStat(0)

        ### start to plot mass distributions
        
        h_rec = ifile.Get(par.selection + '/h_mass')
        h_scin = ifile.Get(par.selection + '/h_mass_scin')
        h_cher = ifile.Get(par.selection + '/h_mass_cher')

        h_rec_tD =  ifile.Get(par.selection + '/h_mass_hyb_ene')

        h_rec_tD.SetXTitle("m_{\gamma\gamma} [GeV]")
        h_rec_tD.Draw()
        h_rec_tD.Fit("gaus","","",120,140)
        l_note = ROOT.TLatex(0.8,0.9,"True direction used for photons")
        l_note.SetNDC()
        c.Print("RecoHiggsMass_trueDir.pdf")
        
        h_rec.SetXTitle("m_{jj} [GeV]")
        h_rec.Draw()
        h_rec.Fit("gaus","","",120,140)
        c.Print("RecoHiggsMass.pdf")

        h_rec.SetFillColor(ROOT.kBlue)
        h_rec.SetLineColor(ROOT.kBlue)
        h_rec.Draw("HIST")
        h_scin.SetLineColor(ROOT.kRed)
        h_cher.SetLineColor(ROOT.kGreen)
        f_rec = h_rec.GetFunction("gaus")
        f_rec.SetLineWidth(3)
        f_rec.SetLineColor(2)
        h_rec.Draw("HIST")
        h_cher.Draw("Esame")
        h_scin.Draw("Esame")
        f_rec.Draw("same")
        
        h_leg = ROOT.TLegend(0.6,0.6,0.8,0.8)
        h_leg.SetShadowColor(10)
        h_leg.SetLineColor(10)
        h_leg.AddEntry(h_rec,"Combined","l")
        h_leg.AddEntry(h_scin,"S-channel","l")
        h_leg.AddEntry(h_cher,"C-channel","l")
        h_leg.Draw()
        
        c.Print("CompareChannels.pdf")

        h_hyb_E =  ifile.Get(par.selection + '/h_mass_hyb_ene')
        h_hyb_dir =  ifile.Get(par.selection + '/h_mass_hyb_dir')
        h_hyb_dir.SetLineColor(ROOT.kGreen)
        h_hyb_E.SetLineColor(ROOT.kCyan)
        h_hyb_dir.Draw("HIST")
        h_hyb_E.Draw("HISTsame")
        h_rec.Draw("same")
        h_leg = ROOT.TLegend(0.15,0.6,0.35,0.8)
        h_leg.SetShadowColor(10)
        h_leg.SetLineColor(10)
        h_leg.AddEntry(h_rec,"Combined","l")
        h_leg.AddEntry(h_hyb_dir,"Using truth energy","l")
        h_leg.AddEntry(h_hyb_E,"Using truth direction","l")
        h_leg.Draw()

        c.Print("HybridPlots.pdf")

        #### Plot "single particle energy" in tower 10

        h_singlePart = ifile.Get(par.selection + '/h_tower10_E_60GeV')
        h_singlePart_cher = ifile.Get(par.selection + '/h_tower10_E_60GeV_cher')
        h_singlePart_scin = ifile.Get(par.selection + '/h_tower10_E_60GeV_scin')
        h_singlePart.GetXaxis().SetRangeUser(0.8,1.2)
        h_singlePart_cher.GetXaxis().SetRangeUser(0.8,1.2)
        h_singlePart_scin.GetXaxis().SetRangeUser(0.8,1.2)
        h_singlePart_cher.SetLineColor(ROOT.kGreen)
        h_singlePart_scin.SetLineColor(ROOT.kRed)
        h_singlePart.SetLineColor(ROOT.kBlue)
        h_singlePart.Fit("gaus","","",0.9,1.1)
        h_singlePart.SetXTitle("R")
        h_singlePart.SetYTitle("Entries")
        h_singlePart.SetMaximum(h_singlePart.GetMaximum() * 1.4)
        h_singlePart.Draw()
        h_singlePart_scin.Draw("same")
        h_singlePart_cher.Draw("same")
        l_lat = ROOT.TLatex(0.125,0.8,"Tower 10 - 57.5 GeV < E < 62.5 GeV")
        l_lat.SetTextFont(72)
        l_lat.SetTextSize(0.035)
        l_lat.SetNDC()
        l_lat.Draw()
        l_leg = ROOT.TLegend(0.2,0.4,0.4,0.6)
        l_leg.AddEntry(h_singlePart_scin,"S","L")
        l_leg.AddEntry(h_singlePart_cher,"C","L")
        l_leg.AddEntry(h_singlePart,"0.5(S+C)","L")
        l_leg.SetLineColor(10)
        l_leg.SetShadowColor(10)
        l_leg.Draw()
        c.Print("Tower10_60GeVResponse.pdf")
        
        h_col_name = ["scint1_R","scint2_R","cher1_R","cher2_R"]

        h_prof_list = []

        #### These are the detailed response histograms
        
        for hname in h_col_name:
            h_col = ifile.Get(par.selection + '/h_col_' + hname)
            h_col.SetXTitle("#theta_{#gamma}")
            h_col.SetYTitle("E_{cher}/E_{#gamma}")
            h_col.Draw("COLZ")
            c.Print(hname + "_col_Response2D.pdf")

            h_finescan = ifile.Get(par.selection + '/h_finephiscan_' + hname)
            h_finescan.SetXTitle("#phi_{#gamma}")
            h_finescan.SetYTitle("E_{cher}/E_{#gamma}")
            h_finescan.Draw("COLZ")
            h_prof = h_finescan.ProfileX("h_profile_" + hname,1,-1,"s")
            h_prof.SetLineColor(ROOT.kRed)
            h_prof.SetLineWidth(3)
            h_prof.Draw("same")
            c.Print(hname+"_finephiscan_Response2D.pdf")
            h_prof.SetXTitle("#phi_{#gamma}")
            h_prof.SetYTitle("E_{cher}/E_{#gamma}")
            h_prof.SetMaximum(1.02)
            h_prof.SetMinimum(0.98)
            h_prof_list.append(h_prof)
            h_prof.Draw()
            c.Print(hname+"_finephiscan_Response_prof.pdf")

            h_finescan = ifile.Get(par.selection + '/h_fineEscan_' + hname)
            h_finescan.SetXTitle("E_{#gamma}")
            h_finescan.SetYTitle("E_{cher}/E_{#gamma}")
            h_finescan.Draw("COLZ")
            h_prof = h_finescan.ProfileX("h_profile_" + hname,1,-1,"s")
            h_prof.SetLineColor(ROOT.kRed)
            h_prof.SetLineWidth(3)
            h_prof.Draw("same")
            c.Print(hname+"_fineEscan_Response2D.pdf")
            h_prof.SetXTitle("#theta_{#gamma}")
            h_prof.SetYTitle("E_{cher}/E_{#gamma}")
            h_prof.SetMaximum(1.02)
            h_prof.SetMinimum(0.98)
            h_prof_list.append(h_prof)
            h_prof.Draw()
            c.Print(hname+"_fineEscan_Response_prof.pdf")

            h_finescan = ifile.Get(par.selection + '/h_finescan_' + hname)
            h_finescan.SetXTitle("#theta_{#gamma}")
            h_finescan.SetYTitle("E_{cher}/E_{#gamma}")
            h_finescan.Draw("COLZ")
            h_prof = h_finescan.ProfileX("h_profile_" + hname,1,-1,"s")
            h_prof.SetLineColor(ROOT.kRed)
            h_prof.SetLineWidth(3)
            h_prof.Draw("same")
            c.Print(hname+"_finescan_Response2D.pdf")
            h_prof.SetXTitle("#theta_{#gamma}")
            h_prof.SetYTitle("E_{cher}/E_{#gamma}")
            h_prof.SetMaximum(1.02)
            h_prof.SetMinimum(0.98)
            h_prof_list.append(h_prof)
            h_prof.Draw()
            c.Print(hname+"_finescan_Response_prof.pdf")

            doDetailedEtaScan = False
            if doDetailedEtaScan:
                    for i in range(0,h_finescan.GetNbinsX()):
                            h1d_finescan = h_finescan.ProjectionY("h1d",i,i+1)
                            h1d_finescan.Fit("gaus");
                            h1d_finescan.Fit("gaus","","",h1d_finescan.GetFunction("gaus").GetParameter(1) - 1.5*h1d_finescan.GetFunction("gaus").GetParameter(2),h1d_finescan.GetFunction("gaus").GetParameter(1) + 1.5*h1d_finescan.GetFunction("gaus").GetParameter(2))
                            h1d_finescan.Draw()
                            c.Print(h_finescan.GetName() + "_h1d_" + str(i) + ".pdf")


                    
        if (par.doCalib2):            
            calfile = ROOT.TFile("calfile_step2_DR_hgamgam_check.root","recreate")
            calfile.cd()
            for h_prof in h_prof_list:
                h_prof.Write()
            calfile.Close()

        h_calib_2D_scint1 = ifile.Get(par.selection + "/h_calib_scint1")
        h_calib_2D_scint2 = ifile.Get(par.selection + "/h_calib_scint2")
        h_calib_2D_cher1 = ifile.Get(par.selection + "/h_calib_cher1")
        h_calib_2D_cher2 = ifile.Get(par.selection + "/h_calib_cher2")
        h_calib_scint1 = h_calib_2D_scint1.ProfileX()
        h_calib_scint2 = h_calib_2D_scint2.ProfileX()
        h_calib_cher1 = h_calib_2D_cher1.ProfileX()
        h_calib_cher2 = h_calib_2D_cher2.ProfileX()
        h_calib_scint1.SetMarkerStyle(20)
        h_calib_scint2.SetMarkerStyle(23)
        h_calib_cher1.SetMarkerStyle(24)
        h_calib_cher2.SetMarkerStyle(32)
        h_axes = ROOT.TH2F("h_axes","",200,0.,1.,100,0.5,1.5)
        h_axes.SetXTitle("E_{tower 2} / E_{tower 1}")
        h_axes.SetYTitle("E_{jet}/E_{\gamma}")
        h_axes.Draw()
        h_calib_scint1.Draw("same")
        h_calib_scint2.Draw("same")
        h_calib_cher1.Draw("same")
        h_calib_cher2.Draw("same")

        h_leg = ROOT.TLegend(0.6,0.6,0.8,0.8)
        h_leg.SetShadowColor(10)                                                                         
        h_leg.SetLineColor(10)                                                                    
        h_leg.AddEntry(h_calib_scint1,"E_{scin1}","p")                                  
        h_leg.AddEntry(h_calib_scint2,"E_{scin2}","p")                                   
        h_leg.AddEntry(h_calib_cher1,"E_{cher1}","p")                                 
        h_leg.AddEntry(h_calib_cher2,"E_{cher2}","p")                                                  
        h_leg.Draw()                     
        
        c.Print("CompareCalib.pdf")

        if (par.doCalib1):
            calfile = ROOT.TFile("calfile_DR_hgamgam_check.root","recreate")
            calfile.cd()
            h_calib_scint1.Write()
            h_calib_scint2.Write()
            h_calib_cher1.Write()
            h_calib_cher2.Write()
            calfile.Close()

        ##### Now produce mass plots for different values of w

        wval_list = ["2.50","2.25","2.00","1.75","1.50","1.25","1.00","0.75","0.50","0.25"]

        h_basename = par.selection + "/h_mass_CoverS_"
        w_res = array('d')
        w_val = array('d')
        
        for w_str in wval_list:
                h_name = h_basename + w_str
                h = ifile.Get(h_name)
                h.Fit("gaus","","",120,130)
                w_res.append(h.GetFunction("gaus").GetParameter(2))
                w_val.append(float(w_str))
        g_wval = ROOT.TGraph(len(w_val),w_val,w_res)
        h_axes = ROOT.TH2F("h_axes_wval","",100,0,2.75,100,1.5,3)
        h_axes.SetXTitle("w")
        h_axes.SetYTitle("#sigma(m) [GeV]")
        h_axes.Draw()
        t_lat0 = ROOT.TLatex(0.2,0.8,"Photon 4-vector defined as #frac{S+wC}{1+w}")
        t_lat0.SetNDC()
        t_lat0.Draw()
        t_lat1 = ROOT.TLatex(0.2,0.7,"w = 0 #rightarrow Use only S")
        t_lat1.SetNDC()
        t_lat1.Draw()
        t_lat2 = ROOT.TLatex(0.2,0.65,"w = #infty #rightarrow Use only C")
        t_lat2.SetNDC()
        t_lat2.Draw()
        g_wval.Draw("PL")

        c.Print("MassWidth_w.pdf")

        ####### Now study the response using the residual direction with respect to the centre of the cell

        h_basename = par.selection + "/h_residual_E_60GeV"
        h = ifile.Get(h_basename)
        h_scin = ifile.Get(h_basename+"_scin")
        h_cher = ifile.Get(h_basename+"_cher")
        h_entries = ifile.Get(h_basename+"_entries")

        h.Divide(h_entries)
        h_scin.Divide(h_entries)
        h_cher.Divide(h_entries)

        h.SetXTitle("#theta [deg]")
        h_scin.SetXTitle("#theta [deg]")
        h_cher.SetXTitle("#theta [deg]")

        h.SetYTitle("#phi [deg]")
        h_scin.SetYTitle("#phi [deg]")
        h_cher.SetYTitle("#phi [deg]")

        h.Draw("COLZ")
        c.Print("h_residualResponse.pdf")
        h.GetListOfFunctions().FindObject("palette").GetAxis().SetWmin(0.7)
        h_scin.Draw("COLZ")
        c.Print("h_residualResponse_scin.pdf")
        h_cher.Draw("COLZ")
        c.Print("h_residualResponse_cher.pdf")
            

if __name__ == "__main__":
    main()


