#include <iostream>
#include <TChain.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <TString.h>

#include <map>

#include "Hgamgam_dummySim.h"

// The entry point for the analysis code

int main(int argc, char **argv) 
{

  Hgamgam_dummySim mysim;
  mysim.RunDecays();
  
  return 0;
}
