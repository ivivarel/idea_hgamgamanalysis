#include <iostream>
#include <TChain.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <TString.h>

#include <map>

#include "HgamgamAnalysis.h"

void help();

std::map<std::string,std::string> m_listArguments;

// The entry point for the analysis code

int main(int argc, char **argv) 
{

  std::cout << "*********************************\n";
  std::cout << "*     Hgamgam    *\n";
  std::cout << "*********************************\n";
  std::cout << "\n\n" << std::endl;

  if (argc < 2) {
    help();
    return -1;
  }

  // Create a chain 

  TString treename = "MyTree";
  
  TChain l_chain(treename);

  // Verify that the input directory exists. If it does, check whether it is a file or a directory and act accordingly
  
  struct stat info;

  if( stat( argv[1], &info ) != 0 ){
    printf( "ERROR! cannot access %s\n", argv[1] );
    return -1;
  }
  
  // Check if the string corresponds to a directory. If not, assume it is a file

  bool is_dir = S_ISDIR(info.st_mode);

  // The directory is there. Prepare the TChain
  
  Int_t nfiles = 0;
  TString input_name = argv[1];

  if (is_dir) {
    std::cout << ( "Running on files in " + input_name + '\n' );
    nfiles = l_chain.Add(input_name + "/*.root*");
  } else {
    std::cout << ( "Running on file " + input_name + '\n' );
    nfiles = l_chain.Add(input_name);
  }
  
  if (nfiles > 0) 
    std::cout << "Number of files added: " << nfiles << std::endl;
  else 
    {
      std::cout << "\n\n\nDid not manage to run on tree " << treename << " from input " << input_name << ". Exiting... " << std::endl;
      return -1;
    }

  // Understand whether we are in debug mode

  TString s_debug = argv[argc-1];
  bool l_debug = !s_debug.CompareTo("debug");
  if (l_debug) std::cout << "\nThe code will run in debug mode\n"; 

  TString l_outname = "Output";
  
  if (argc > 2 && !l_debug){
    // Assume that a second arg which is not debug is the output name
    l_outname = argv[3];
  }

  HgamgamAnalysis * l_analysis = new HgamgamAnalysis(l_outname,l_debug);

  if (!l_analysis->Init(&l_chain)){
    std::cerr << "Cannot initialise analysis" << std::endl;
    return -1;
  }

  if (!l_analysis->Loop()){
    std::cerr << "Problems in the event loop" << std::endl;
    return -1;
  }

  delete l_analysis;
  
  return 0;
}

void help()
{
  std::cout << "USAGE: Hgamgam [INPUTDIR] [OUTFILENAME] [debug]" << std::endl;
  std::cout << "\nThis is the meaning of the arguments:" << std::endl;
  std::cout << "\nINPUTDIR: A chain will be built with files matching '*.root*' in INPUTDIR" << std::endl;
  std::cout << "OUFILENAME: Two output files will be produced - OUTFILENAME_HIST.root will contain histograms, while OUTFILENAME_NTUP.root will contain a skimmed ntuple" << std::endl;
  std::cout << "debug: If the string 'debug' is specified, the code will be run in with a verbose output" << std::endl;
  std::cout << "\nOnly the INPUTDIR is mandatory" << std::endl;
}

  
