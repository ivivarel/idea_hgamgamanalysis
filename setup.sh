#! /bin/bash

BASEDIR=$PWD

export PYTHONPATH=${BASEDIR}/scripts:${PYTHONPATH}

export PATH=${BASEDIR}:${PATH}
