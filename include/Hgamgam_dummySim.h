#ifndef HGAMGAM_DUMMYSIM_H
#define HGAMGAM_DUMMYSIM_H

#include <TRandom3.h>
#include <TFile.h>

class Hgamgam_dummySim
{
 public:
  Hgamgam_dummySim(TString outfilename = "Hgamgam_dummySim_output.root", double astoc = 0.11, double bconst = 0.01);
  ~Hgamgam_dummySim(){};
  double E_sigma(double E);
  bool RunDecays(unsigned int nevents =1000000);
  void SetResolutionStocasticTerm(double astoc) {m_a = astoc;}
  void SetResolutionConstantTerm(double astoc) {m_a = astoc;}
  void SetOutFileName(TString outfilename) {m_outfilename = outfilename;}

 private:
  TString m_outfilename;
  TFile * m_file;
  double m_a;
  double m_b;
  TRandom3 m_random;
};

#endif // #ifndef HGAMGAM_DUMMYSIM_H
